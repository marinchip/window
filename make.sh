#!/bin/sh

# select drivers
echo Selecting termin-emu and termout-emu as drivers
cp drivers/termin-emu src/termin.bas
cp drivers/termout-emu src/termout.bas

# compile QBasic files, place in link
qb_files="command control screen termin termout"

cd src
for i in $qb_files
do
   echo link/$i.rel=src/$i.bas
    ../../emu/host 1/qbasic ../link/$i.rel=$i.bas
done
rm temp1$ termin.bas termout.bas

# assemble support files, place in link
asm_files="linedb fileio insubs textin textout"

for i in $asm_files
do
   echo link/$i.rel=src/$i
   ../../emu/host 1/asm ../link/$i.rel=$i.asm
done

# link Window, executable is 'w'
echo Linking Window...
cd ../link
../../emu/host 1/link <<EOF
out w
in control.rel,linedb.rel,screen.rel,termin.rel,termout.rel
in command.rel
in fileio.rel,insubs.rel,textin.rel,textout.rel
in ../qlib/baslib.rel
in ../qlib/strings.rel
in ../qlib/optsubs.rel
in ../qlib/basio.rel
in ../qlib/match.rel
in ../qlib/optsubc.rel
in ../qlib/optsuba.rel
in ../qlib/arrays.rel
in ../qlib/hardware.rel
in ../qlib/dumfcl.rel
in ../qlib/backstop.rel
map
end
EOF

#
# move window to top dir and clean-up link dir
mv w ..

#rm *.rel


